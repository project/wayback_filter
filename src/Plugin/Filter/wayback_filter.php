<?php
/**
 * @file
 * Contains Drupal\wayback\Plugin\Filter\wayback_filter
 */

namespace Drupal\wayback_filter\Plugin\Filter;
/* namespace er Drupal\modulnavn\Plugin\Filter; - eller ogsaa er det klassenavn*/

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * id er modulnavn, filnavn og klassenavn  - som er det samme
 *
 * @Filter(
 *   id = "wayback_filter",
 *   title = @Translation("Wayback Filter"),
 *   description = @Translation("Add Wayback links to existing links in older nodes."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class wayback_filter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // v($text, 0 , 'body text');
    $result = new FilterProcessResult($text); // If we have to return something unaltered
    $arg = explode('/', \Drupal\Core\Url::fromRoute('<current>')->getInternalPath()); // Give me back my old functions

    // If not showing a node in full, simply return
    if (empty($arg[1]) || !empty($arg[2]) && ($arg[1] != 'node' && !is_numeric($arg[1]))) {
      return $result;
    }

    // Get the age of the node
    $created = \Drupal::database()->select('node_field_data', 'n')->fields('n', ['created]'])->condition('nid', $arg[1])->execute()->fetchAssoc();

    // When to employ Wayback links?
    $waybacklink_start_date = strtotime('-' . $this->settings['waybacklink_start'] . ' years');

    // Don't bother with new nodes
    if ($created['created'] > $waybacklink_start_date) {
      // v('too new');
      return $result;
    }

    // Find <a codes
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
    preg_match_all("/$regexp/siU", $text, $links); // stuff all the links in the text into $links array
    $done = array();
    // v($links,0,'links in the body text');

    // Go through each
    foreach ($links[0] as $orig_code) {
      // Find hrefs
      preg_match('/href="([^"]*)"/i', $orig_code, $href);

      if (substr($href[1], 0, 22) == 'https://web.archive.org') {
        // Do not bother waybacking Wayback links
        continue;
      }

      if ($href && !isset($done[$href[1]])) {
        // Work on http(s):// refs only
        if (substr($href[1], 0, 7) == 'http://' || substr($href[1], 0, 8) == 'https://') {
          // Create wayback link
          $time = date('YmdHis', $created['created']); // $node->created
          $wayback_link = 'https://web.archive.org/web/' . $time . '/' . $href[1];
          $wayback = $orig_code . ' <a title="' . $this->settings['waybacklink_title'] .'" class="wayback-link" href="' . $wayback_link . '"><span class = "waybacklink_icon">' . $this->settings['waybacklink_icon'] . '</span></a>';
          // Replace with added Wayback-link
          $text = str_replace($orig_code, $wayback, $text);
        }
        // Avoid repeated replaces
        $done[$href[1]] = TRUE;
      }
    }
      $result = new FilterProcessResult($text);
      return $result;
}

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Set a default value for the field. Even if the filter is not enabled for a text format and the wayback filter settings form is invisible, Drupal will kvetch about empty values
    if(empty($this->settings['waybacklink_icon'])) {
      $this->settings['waybacklink_icon'] = '🏛️';
    }
    $form['waybacklink_icon'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#title' => $this->t('Icon or text for the Wayback link'),
      '#default_value' => $this->settings['waybacklink_icon'],
      '#description' => $this->t('Enter an emoji or some text.<br />We recommend <a href="@classical_building">🏛️</a> (classical building), <a href="@floppy">💾</a> (floppy disk) or simply the text <em>[Wayback link]</em>.<br />You can find emojis at <a href = "@emojipedia">Emojipedia</a>', array('@classical_building' => 'https://emojipedia.org/search/?q=building', '@floppy' => 'https://emojipedia.org/search/?q=floppy', '@emojipedia' => 'https://emojipedia.org/')),
    );

    if(empty($this->settings['waybacklink_title'])) {
      $this->settings['waybacklink_title'] = $this->t('Click to see an archived version of the destination page.');
    }
    $form['waybacklink_title'] = array(
      '#type' => 'textfield',
      '#size' => 40,
      '#title' => $this->t('Link title for the Wayback link'),
      '#default_value' => $this->settings['waybacklink_title'],
      '#description' => $this->t('This is what desktop users gets to read when their cursor hovers over the Wayback link.'),
    );

    if(empty($this->settings['waybacklink_start'])) {
      $this->settings['waybacklink_start'] = 3;
    }
    $form['waybacklink_start'] = array(
      '#type' => 'number',
      '#title' => $this->t('Place Wayback links on nodes older than...'),
      '#min' => 1,
      '#max' => 30,
      '#field_suffix' => 'years',
      '#default_value' => $this->settings['waybacklink_start'],
      '#description' => $this->t('Nodes older than this many years will get a trailing Wayback link.'),
    );

    $form['waybacklink_preview'] = array(
      '#markup' => '<label>' . $this->t('Preview of a link with a trailing Wayback link') . '</label><a href = "https://vertikal.dk/linkrot-solved-problem">Linkrot is a solved problem</a> <a href = "https://web.archive.org/web/20210701194421/https://vertikal.dk/linkrot-solved-problem" title = "waybacklink_title" class = "wayback-link" id = "waybacklink_preview">[Wayback link]</a>',
      '#allowed_tags' => array('label', 'a'),
    );

    /*
    $form['archivemdlinkicon'] = array(
          '#type' => 'textfield',
          '#size' => 20,
          '#title' => $this->t('Icon or text for the Archive Today'),
          '#default_value' => '',
          '#description' => $this->t('If it remains empty, it'll not be used. Enter a Unicode number (i.e. U+1F3DB) or some text.<br />We recommend <em><a href=":classic_building">U+1F3DB</a></em> (classical building), <em><a href=":floppy">U+1F3DB</a></em> (floppy disk) or simply the text <em>Click here for an archived version of this link</em>.<br />You can find Unicode numbers and emojis (with Unicode numbers) at <a href = ":unicode">Unicode-table.com</a>', array(':classical_building' => 'https://unicode-table.com/en/1F3DB/', ':floppy' => 'https://unicode-table.com/en/1F5AB/', ':unicode' => 'https://unicode-table.com/en/')),
        );

        $form['archivemdlinktitle'] = array(
          '#type' => 'textfield',
          '#size' => 40,
          '#title' => $this->t('Link title for the Archive Today link'),
          '#default_value' => $this->t('Click to see an archived version of the destination page.'),
          '#description' => $this->t('This is what desktop users gets to read when their cursor hovers over the Wayback link.'),
        );

        $form['archivemdpreview'] = array(
          '#title' => $this->t('Preview of a link with a trailing Archive Today link'),
          '#markup' => '<a href = "https://vertikal.dk/linkrot-solved-problem">Linkrot is a solved problem</a> <a href = "https://archive.md/https://vertikal.dk/linkrot-solved-problem"  title = "archivemdlinktitle "class = "wayback-link">[]</a>"',
        );
    */

    $form['waybacklink_help'] = array(
      '#markup' => '<br /><br /><label>' . $this->t('Advanced help') . '</label>' . $this->t('Most people will be able to see emojis and unicode characters. Even if they differ a bit between platforms. Only desktop users will be able to see the link title and only when their cursor hovers over the Wayback link. The Wayback link can be styled by adding styles to the css class <em>wayback-link</em> (i.e <em>.wayback-link a {font-size: 0.8rem; color: red;}</em> somewhere in your stylesheet will make the Wayback link small and red.<br />If you wish to paste actual emojis into any of the above fields, you better make sure that your Drupal database is <a href="@utf8mb4">utf8mb4</a>. Seeing as you are running this on a Drupal 8 or 9, it probably is.', array('@utf8mb4' => 'https://www.eversql.com/mysql-utf8-vs-utf8mb4-whats-the-difference-between-utf8-and-utf8mb4/')),
      '#allowed_tags' => array('label', 'a', 'br'),
      );

    return $form;
  }
}

// TODO make css work
// TODO make sure it's utf8mb4 before installing
// TODO drupal coding standards must be kept
// TODO new git user at drupal.org must be made
/*


Please, I don't want this module to kill my site
It will not kill your site as long as your database tables are utf8mb4 encoded. 99,5% of all Drupal 8 and 9 installation run on utf8mb4 databases. So you should have no problems.

How do I install it?
As any other module

How do I set it up well?
1. Go to /admin/config/content/formats and choose a text format where you're keen on getting Wayback links trailing the existing links.
2. Enable Wayback filter and place it as one of the last filters.
3. Make it look great using the filter settingsForm. If you're very keen to see it in action, you can set the "Place Wayback links on nodes older than..." to 1 year.

How do I test it?
1. Go to a very old node (with some links in the body) employing the text format you've just set up. It'll feature show Wayback Links trilling the existing links.
2. Rejoice or
2. Flush your caches and rejoice (with Drupal 8+ you can never flush your caches too often).

What if the Wayback links do not show?
1. The node you're looking at is maybe not that old? Edit it to find out. If you wish, you can temporarily change the date of the node to 01.01.2000.
2. The node might not be using the text format you just spent some time setting up so well with Wayback links. Choose another node or make a new node with an old date from scratch.

How do I make the trailing Wayback links look even more sexy?
Add <em>.wayback-link a {font-size: 4rem;}</em> somewhere in your stylesheet. Flush some caches. Using the Wayback filter settings form and css, you should be able to make it look awesome.

The great External Link module adds an icon to my Wayback links. I don't want that!
Add .wayback-link svg {display: none;} to your style sheet. Another (and most probably inferior) way is to go to /admin/config/user-interface/extlink click Pattern Matching and, under Exclude links matching the pattern, write web\.archive\.org . That way


*/





