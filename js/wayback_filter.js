(function($) {
    $(document).ready(function () {
        var icon = $('#edit-filters-wayback-filter-settings-waybacklink-icon').val();
        var title = $('#edit-filters-wayback-filter-settings-waybacklink-title').val();
        console.log(title);
        $('#waybacklink_preview').text(icon);
        $('#waybacklink_preview').attr('title',title);
        $('#edit-filters-wayback-filter-settings-waybacklink-icon').change(function () {
            var icon = $('#edit-filters-wayback-filter-settings-waybacklink-icon').val();
            var title = $('#edit-filters-wayback-filter-settings-waybacklink-title').val();
            $('#waybacklink_preview').text(icon);
            $('#waybacklink_preview').attr('title',title);
        });
        $('#edit-filters-wayback-filter-settings-waybacklink-title').change(function () {
            var icon = $('#edit-filters-wayback-filter-settings-waybacklink-icon').val();
            var title = $('#edit-filters-wayback-filter-settings-waybacklink-title').val();
            $('#waybacklink_preview').text(icon);
            $('#waybacklink_preview').attr('title',title);
        });
    });
})(jQuery, Drupal);